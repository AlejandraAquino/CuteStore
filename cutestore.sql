-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-07-2017 a las 18:02:37
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cutestore`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `codigo_comentario` int(11) NOT NULL,
  `codigo_usuariocliente` int(11) NOT NULL,
  `nombre_comentario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion_comentario` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `codigo_producto` int(11) NOT NULL,
  `valoracion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_productos`
--

CREATE TABLE `estados_productos` (
  `codigo_estadoproducto` int(11) NOT NULL,
  `nombre_estadoproducto` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `codigo_factura` int(10) UNSIGNED NOT NULL,
  `total_factura` int(11) NOT NULL,
  `codigo_usuarioclientes` int(11) NOT NULL,
  `fecha_factura` date NOT NULL,
  `estado_factura` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `codigo_pedidos` int(10) UNSIGNED NOT NULL,
  `codigo_factura` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha_dedido` date NOT NULL,
  `codigo_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `codigo_producto` int(10) UNSIGNED NOT NULL,
  `nombre_producto` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `precio` decimal(5,2) NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_tipoproducto` int(11) NOT NULL,
  `codigo_proveedor` int(11) NOT NULL,
  `existencia` int(11) NOT NULL,
  `codigo_estadoproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`codigo_producto`, `nombre_producto`, `precio`, `description`, `imagen`, `codigo_tipoproducto`, `codigo_proveedor`, `existencia`, `codigo_estadoproducto`) VALUES
(1, 'Oso panda', '20.00', 'oso panda con un sombrero rosa', '1498487035.jpeg', 1, 1, 30, 1),
(3, 'Oso Pooh y sus amigos', '10.00', 'Lindo Oso pooh', '1498490979.jpg', 2, 2, 20, 1),
(5, 'OSOOO', '20.00', 'OSOOO', '1498537790.jpg', 3, 1, 20, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `codigo_proveedor` int(10) UNSIGNED NOT NULL,
  `nombre_proveedor` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `telefono_proveedor` int(8) NOT NULL,
  `email_proveedor` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`codigo_proveedor`, `nombre_proveedor`, `telefono_proveedor`, `email_proveedor`) VALUES
(1, 'Lola', 75896525, 'lolamarr@gmail.com'),
(2, 'Mariana', 74512458, 'maria@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_productos`
--

CREATE TABLE `tipo_productos` (
  `codigo_tipoproducto` int(10) UNSIGNED NOT NULL,
  `tipo_producto` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_productos`
--

INSERT INTO `tipo_productos` (`codigo_tipoproducto`, `tipo_producto`, `description`) VALUES
(1, 'Pandas', 'ojos parchados de negro y cuerpo blanco'),
(2, 'Personajes de Osos', 'Tipos de osos que son de programas infantiles'),
(3, 'Osos reales', 'se apegan lo mayor posible a la realidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuarios`
--

CREATE TABLE `tipo_usuarios` (
  `codigo_tipousuario` int(10) UNSIGNED NOT NULL,
  `tipo_usuario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_usuarios`
--

INSERT INTO `tipo_usuarios` (`codigo_tipousuario`, `tipo_usuario`, `description`) VALUES
(1, 'Administrador', 'Maneja todo lo de eliminar, modificacion del sistema en completo'),
(2, 'Gerente', 'Solo maneja comentarios, clientes y productos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `codigo_usuario` int(10) UNSIGNED NOT NULL,
  `nombres_usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos_usuario` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_usuario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email_usuario` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_tipousuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`codigo_usuario`, `nombres_usuario`, `apellidos_usuario`, `nombre_usuario`, `email_usuario`, `clave`, `codigo_tipousuario`) VALUES
(3, 'Alejandra', 'Marroquin', 'Ale', 'Ale.Marr@gmail.com', '$2y$10$aNnTyiSk6bk5Y0Y0zt7igOUnYUP4d8Xv4HcPS5XQ6TYFXm7C4qoiu', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_clientes`
--

CREATE TABLE `usuarios_clientes` (
  `codigo_usuariocliente` int(10) UNSIGNED NOT NULL,
  `nombres_clientes` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos_clientes` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_usuariocliente` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email_cliente` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `numero_telefonico` int(8) NOT NULL,
  `clave` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios_clientes`
--

INSERT INTO `usuarios_clientes` (`codigo_usuariocliente`, `nombres_clientes`, `apellidos_clientes`, `nombre_usuariocliente`, `email_cliente`, `direccion`, `numero_telefonico`, `clave`) VALUES
(1, 'Edgard', 'Aquino', 'Ed', 'Ed.aquino@gmail.com', 'Cuscatlan', 78569584, '$2y$10$.sfZMrLWclhzn3DRm5DKjucsr71KwZpaRlsBn.3.PXjKBDkJnb34K'),
(3, 'Maria', 'Carmen', 'Car', 'maria@gmail.com', 'Cusca', 74589652, '$2y$10$79c3TCvO9Q4M9ZRDt.caWOIOyuuYsn6keha9AZ/y3.KlKTMBdS.V6'),
(4, 'Marlena', 'Martinez', 'Marti', 'marti@gmail.com', 'Cuscatlan', 78564589, '$2y$10$I7IqZvMpQUcOEO1ON5jEPuD3B6aV7VbOVGN9UwlgHfT.O8xMYVN6C'),
(5, 'Javier', 'Campos', 'Jav', 'javi.cam@gmail.com', 'Santa Teresa', 74512635, '$2y$10$NzzJZ0fy5WqMpYrogTjGy.QuyR.klkenKRWYHe7JoXSiBSXiGP7pu'),
(7, 'Maricela', 'Peraza', 'Pera', 'pera@gmail.com', 'Cuscatlan', 71256354, '$2y$10$aXu8Y1Gerb.OXTHUrV7hH.lDNyGjpzL3EBDyxVNJXfD734NQNVHBm'),
(8, 'Johanna', 'Ramoz', 'Joha', 'Joha.ramos@gmail.com', 'Cuscatancingo', 74512365, '$2y$10$B5mMGnpqSdtOrbbJw8/oH.44VUPvOec9dR17qcFoTA3uGv5PUUjqu'),
(10, 'Joel', 'Martinez', 'JoMar', 'Jo@gmail.com', 'Cuscatlan', 74125415, '$2y$10$GzZlL8g4qVr.GlfvBjCRxObtR4GBppiLrlyGypjkxDUTVVWB1s1w6'),
(11, 'Ana', 'Lira', 'Anita', 'ana.lira@gmail.com', 'San Miguel', 74512458, '$2y$10$45o03Sk4HFeY3Oed6Xp7m.AfhgxBLK5mwarAJ.sjLAra20AejwEsi'),
(12, 'Juan', 'Ortiga', 'Juti', 'ju.ortiga@gmail.com', 'San salvador', 74152654, '$2y$10$9BjBwexggvCQKapxDUX8f.n/MS5YGG9W6jcl55B5qsgeamQSFUWKi'),
(13, 'Mario', 'Miranda', 'Mamira', 'mario.miranda@gmail.com', 'San salvador', 74589658, '$2y$10$Ggpei02ZCyqizR1bkBq8EubBWxs2PnG/u7YKs782RrK8HGQDsXN5W'),
(14, 'Miguel', 'Marcelo', 'Migue', 'migue.mar@gmail.com', 'Soyapango', 74154575, '$2y$10$U759LPjn8.Kr33B9mv5nU.RDIr6bnRGa7uft2jmGNSdrftQohktKW');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`codigo_comentario`),
  ADD KEY `codigo_producto` (`codigo_producto`),
  ADD KEY `codigo_usuariocliente` (`codigo_usuariocliente`);

--
-- Indices de la tabla `estados_productos`
--
ALTER TABLE `estados_productos`
  ADD PRIMARY KEY (`codigo_estadoproducto`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`codigo_factura`),
  ADD KEY `codigo_factura` (`codigo_factura`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`codigo_pedidos`),
  ADD KEY `codigo_producto` (`codigo_producto`),
  ADD KEY `codigo_factura` (`codigo_factura`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`codigo_producto`),
  ADD KEY `codigo_tipoproducto` (`codigo_tipoproducto`),
  ADD KEY `codigo_proveedor` (`codigo_proveedor`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`codigo_proveedor`);

--
-- Indices de la tabla `tipo_productos`
--
ALTER TABLE `tipo_productos`
  ADD PRIMARY KEY (`codigo_tipoproducto`),
  ADD UNIQUE KEY `tipo_producto` (`tipo_producto`);

--
-- Indices de la tabla `tipo_usuarios`
--
ALTER TABLE `tipo_usuarios`
  ADD PRIMARY KEY (`codigo_tipousuario`),
  ADD UNIQUE KEY `tipo_producto` (`tipo_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`codigo_usuario`),
  ADD UNIQUE KEY `nombre_usuario` (`nombre_usuario`),
  ADD UNIQUE KEY `email_usuario` (`email_usuario`),
  ADD KEY `codigo_tipousuario` (`codigo_tipousuario`);

--
-- Indices de la tabla `usuarios_clientes`
--
ALTER TABLE `usuarios_clientes`
  ADD PRIMARY KEY (`codigo_usuariocliente`),
  ADD UNIQUE KEY `nombre_usuariocliente` (`nombre_usuariocliente`),
  ADD UNIQUE KEY `email_cliente` (`email_cliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `codigo_comentario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estados_productos`
--
ALTER TABLE `estados_productos`
  MODIFY `codigo_estadoproducto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `codigo_factura` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `codigo_pedidos` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `codigo_producto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `codigo_proveedor` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipo_productos`
--
ALTER TABLE `tipo_productos`
  MODIFY `codigo_tipoproducto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tipo_usuarios`
--
ALTER TABLE `tipo_usuarios`
  MODIFY `codigo_tipousuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `codigo_usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios_clientes`
--
ALTER TABLE `usuarios_clientes`
  MODIFY `codigo_usuariocliente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
