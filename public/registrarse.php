<?php
require("page.php");
Page::header("Registrarse");

if(empty($_GET['id'])) 
{
    $id = null;
    $nombres = null;
    $apellidos = null;
    $correo = null;
    $alias = null;
    $telefono = null;
    $direccion = null;
}
else
{
    Page::header("Editar Perfil");
    $id = $_GET['id'];
    $sql = "SELECT * FROM usuarios_clientes WHERE codigo_usuariocliente = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_clientes'];
    $apellidos = $data['apellidos_clientes'];
    $alias = $data['nombre_usuariocliente'];
    $correo = $data['email_cliente'];
    $direccion = $data['direccion'];
    $telefono = $data['numero_telefonico'];
}

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres'];
  	$apellidos = $_POST['apellidos'];
    $correo = $_POST['correo'];
    $direccion = $_POST['direccion'];
    $telefono = $_POST['telefono'];

    try 
    {
        if($nombres != "" && $apellidos != "")
        {
            if($correo != "")
            {
                if($telefono != "")
                {
                    if($direccion != "")
                {
                    if($id == null)
                    {
                        $alias = $_POST['alias'];
                    if($alias != "")
                    {
                        $clave1 = $_POST['clave1'];
                        $clave2 = $_POST['clave2'];
                        if($clave1 != "" && $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO usuarios_clientes (nombres_clientes, apellidos_clientes, nombre_usuariocliente, email_cliente, direccion, numero_telefonico, clave) VALUES(?, ?, ?, ?, ?, ?, ?)";
                                $params = array($nombres, $apellidos, $alias, $correo, $direccion, $telefono, $clave);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un nombre de usuario");
                    }
                }
                else
                {
                    $sql = "UPDATE usuarios_clientes SET nombres_clientes = ?, apellidos_clientes = ?, email_cliente = ?, direccion = ?, numero_telefonico = ? WHERE codigo_usuariocliente = ?";
                    $params = array($nombres, $apellidos, $correo, $direccion, $telefono, $id);
                }
                Database::executeRow($sql, $params);
                header("location: index.php");
                }
                else
                {
                    throw new Exception("Debe ingresar una direccion"); 
                }
            }
            else
            {
                throw new Exception("Debe ingresar un telefono"); 
            }
        }
        else
        {
            throw new Exception("Debe ingresar un correo electrónico");
        }
    }
    else
    {
        throw new Exception("Debe ingresar nombre completo");
    }
}       
        catch (Exception $error)
        {
            Page::showMessage(2, $error->getMessage(), null);
        }
}
else
{
    $nombres = null;
    $apellidos = null;
    $alias = null;
    $correo = null;
    $telefono = null;
    $direccion = null;
}
?>
<h1 class='center'>Registrarse</h1>
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='Cam1' type='text' name='nombres' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='Cam1' type='text' name='apellidos' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos'>Apellidos</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='Cam1' type='text' name='alias' class='validate' <?php print("value='$alias' "); print(($id == null)?"required":"disabled"); ?>/>
            <label for='alias'>Alias</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='email_usuario' type='email' name='correo' class='validate' value='<?php print($correo); ?>' required/>
            <label for='correo'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>location_on</i>
            <input id='Cam1' type='text' name='direccion' class='validate' value='<?php print($direccion); ?>' required/>
            <label for='direccion'>Direccion</label>
        </div>
        <div class='input-field col s12 m6' method=post onSubmit="return validar(this)">
            <i class='material-icons prefix'>call</i>
            <input id='Cam2' type="text" maxlength="8" size="8" name="telefono" class='validate' value='<?php print($telefono); ?>' required/>
            <label for='telefono'>Telefono</label>
        </div>
    </div>
    <?php
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='login.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
 	    <button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
    </div>
</form>

<?php
Page::footer();
?>