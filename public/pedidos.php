<?php
require("page.php");
Page::header("");

    $id = $_GET['id'];
    $sql = "SELECT * FROM productos WHERE codigo_producto='$id'";
    $data = Database::getRow($sql, null);
    $nombre = $data ['nombre_producto'];
    $precio = $data['precio'];
    $existencia = $data['existencia'];

    try
    {
             print("
             <div class= 'container'>
             <div class = 'row'>
             <h1 class='center'>Producto</h1>
             <div class= 'col s6'>
                <ul class='collection with-header'>
                    <li class='collection-header'><h4>$nombre<a class='secondary-content'><span class='icon-newspaper'></span></a></h4></li>
                    <li class='collection-item'>Precio: $precio<a class='secondary-content'><span class='icon-price-tag'></span></a></li>
                    <li class='collection-item'>Cantidad Disponible: $existencia <a class='secondary-content'><span class='icon-stack'></span></a></li>
                    <li class='collection-item'>Pago: Cuando se Entrege <a class='secondary-content'><span class='icon-credit-card'></span></a></li>
                    <li class='collection-item'> Envio: Domicilio <a class='secondary-content'><span class='icon-location'></span></a></li>
                </ul>
            </div>");
    $_POST = validator::validateForm($_POST);
    $cantidad = $_POST['cantidad'];
    $total = $cantidad*$precio;       
        if(isset($_POST['action']))
        {
            $_POST = validator::validateForm($_POST);
            $fecha = date('Y/m/d'); //varibale para guardar la fecha
            $estado = 1; //estado de la factura, es 1 por que no se ha pagado
            $cliente = $_SESSION['codigo_usuariocliente'];
            
            
            try
            {
                if(isset($_SESSION['compra'])) //si ya existe una factura
                {
                        $sql1 = "SELECT codigo_factura FROM factura WHERE codigo_usuariocliente = ?";
                        $params1 = array($cliente);
                        $data1 = Database::getRow($sql1, $params1);
                        $idfactura = $data1['codigo_factura'];
                        $sql="INSERT INTO pedidos(codigo_factura, cantidad, fecha_dedido, codigo_producto) VALUES(?, ?, ?, ?)";
                        $params =array($idfactura, $cantidad, $fecha, $id);
                        if(Database::executeRow($sql, $params))
                        {
                            Page::showMessage(1, "Se te Agrego a tu Carrito De Compras", "");
                            $_SESSION['compra'] = $data1['codigo_factura'];
                        }
                        else
                        {
                            throw new Exception(Database::$error[1]);
                        }
                }
                else
                {
                    $sql2 = "INSERT INTO factura(total_factura, codigo_usuariocliente, fecha_factura, estado_factura) VALUES(?, ?, ?, ?)";
                    $params2= array($total, $cliente, $fecha, $estado);
                    if(Database::executeRow($sql2, $params2))
                    {
                        $sql1 = "SELECT codigo_factura FROM factura WHERE codigo_usuariocliente = ? ORDER By codigo_factura DESC LIMIT 1";
                        $params1 = array($_SESSION['codigo_usuariocliente']);
                        $data1 = Database::getRow($sql1, $params1);
                        $idfactura = $data1['codigo_factura'];
                        $sql="INSERT INTO pedidos(codigo_factura, cantidad, fecha_dedido, codigo_producto) VALUES(?, ?, ?, ?)";
                        $params =array($idfactura, $cantidad, $fecha, $id);
                        if(Database::executeRow($sql, $params))
                        {
                            Page::showMessage(1, "Se creo un Carrito de Compras", "");
                            $_SESSION['compra'] = $data1['codigo_factura'];
                        }
                        else
                        {
                            throw new Exception(Database::$error[1]);
                        }
                        
                    }
                        else
                        {
                            throw new Exception(Database::$error[1]);
                        }
                }
            }
            catch(Exception $error)
            {
                Page::showMessage(2, $error->getMessage(), null);
            }
        }
        print("  <div class='col s2'>
        <form  method='post'>   
        ");
            if(isset($_SESSION['nombre_usuariocliente']))
            {
                    print("
                    <div class='input-field col s12 m6 center'>
                        <input id='cantidad' type='number' name='cantidad' class='validate' max='$existencia' min='1' step='any' value='<?php print($cantidad); ?>' required/>
                        <label for='cantidad'>Cantidad</label>
                    </div>
                    <button class='btn waves-effect green darken -3' type='submit' name='action'><i class='material-icons prefix'>shopping_cart</i>Agregar al Carrito</button>
                    ");                      
            }
            else
            {
                print("
                    <a href='login.php'>
                        <a class='black-text'><i class='material-icons large '>shopping_cart</i></a>
                        <button class='btn waves-effect waves-light green darken -3' type='submit' name='action'>Agregar al Carrito</button>
                    </a>
                ");
            }
        print("
        </form>
        </div>
    </div>
    </div>
        ");
}
catch(Exception $error)
{
    Page::showMessage(2, $error->getMessage(), "productos.php");
}
Page::footer();
?>