<?php
require("page.php");
Page::header("Iniciar Sesión");

if(isset($_SESSION['codigo_usuariocliente']))
{
header("location: index.php");
}

if(!empty($_POST))
{
	$_POST = validator::validateForm($_POST);
  	$alias = $_POST['alias'];
  	$clave = $_POST['clave'];
  	try
    {
      	if($alias != "" && $clave != "")
  		{
  			$sql = "SELECT * FROM usuarios_clientes WHERE nombre_usuariocliente = ?";
		    $params = array($alias);
		    $data = Database::getRow($sql, $params);
		    if($data != null)
		    {
		    	$hash = $data['clave'];
		    	if(password_verify($clave, $hash)) 
		    	{
			    	$_SESSION['codigo_usuariocliente'] = $data['codigo_usuariocliente'];
			      	$_SESSION['nombre_usuariocliente'] = $data['nombres_clientes']." ".$data['apellidos_clientes'];
			      	header("location: index.php");
				}
				else 
				{
					throw new Exception("La clave ingresada es incorrecta");
				}
		    }
		    else
		    {
		    	throw new Exception("El alias ingresado no existe");
		    }
	  	}
	  	else
	  	{
	    	throw new Exception("Debe ingresar un alias y una clave");
	  	}
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<h2 class='center'>Iniciar Sesion</h2>
<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>person_pin</i>
			<input id='alias' type='text' name='alias' class='validate' required/>
	    	<label for='alias'>Usuario</label>
		</div>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>security</i>
			<input id='clave' type='password' name='clave' class="validate" required/>
			<label for='clave'>Contraseña</label>
			<a class="black-text text-lighten-3" href="restablecer.php">¿Haz olvidado tu contraseña?</a>
		</div>
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
</form>
    <div class='row right-align'>
		<button type='submit' class='btn waves-effect blue-grey lighten-1'><a href='registrarse.php' class='white-text'>Registrarse</a></button>
	</div>
<h1 class="header center"><img src="../img/logo2.png" width="300" height="250"></h1>
<?php
Page::footer();
?>