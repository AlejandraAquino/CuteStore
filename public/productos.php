<?php
require("page.php");
Page::header("");

 include("../inc/facebook.php");
 ?>

<!--Bloque de texto donde se encuentran los osos con descripcion y precio-->
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container ">
        <br><br>
        <h1 class="header right"><img src="../img/logo1.png" width="300" height="250"></h1>
        <div class="row center">
          <h1 class="header col s12">Nuestros Productos </h1>
        </div>
        <br><br>
      </div>
    </div>

    <div class="parallax"><img src="../img/OsosTeddy.jpeg" alt="Unsplashed background img 1"></div>
  </div>

    <div class='container' id='productos'>
	<?php
	$sql = "SELECT * FROM tipo_productos";
	$categorias = Database::getRows($sql, null);
	if($categorias != null)
	{
		foreach($categorias as $categoria) 
		{
			$sql = "SELECT * FROM productos WHERE codigo_tipoproducto = ? AND codigo_estadoproducto = 1";
			$params = array($categoria['codigo_tipoproducto']);
			$productos = Database::getRows($sql, $params);
			if($productos != null)
			{
				print("<h4 class='center-align brown-text'>$categoria[tipo_producto]</h4>");
				print("<div class='row'>");
				foreach($productos as $producto) 
				{
					print("
						<div class='card hoverable col s12 m6 l4'>
							<div class='card-image waves-effect waves-block waves-light'>
								<img class='activator' src='../img/productos/$producto[imagen]'>
							</div>
							<div class='card-content'>
								<span class='card-title activator grey-text text-darken-4'>$producto[nombre_producto]<i class='material-icons right'>more_vert</i></span>
							</div>
							<div class='card-reveal'>
								<span class='card-title grey-text text-darken-4'>$producto[nombre_producto]<i class='material-icons right'>close</i></span>
								<p>$producto[description]</p>
								<p>Precio (US$) $producto[precio]</p>
								<a href='pedidos.php?id=".$producto['codigo_producto']."' class='blue-text'><i class='material-icons'>mode_edit</i> Agregar al Carrito</a>
							</div>
						</div>
					");
				}
				print("</div>");
			}
		}
	}
	else
	{
		print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay Productos disponibles</div>");
	}
	?>
	</div><!-- Fin de container -->

<?php
Page::footer();
?>