<?php
require("page.php");
Page::header("Mi Carrito de Compras");

if(!empty($_POST))
{
	$sql = "SELECT p.nombre_producto, e.cantidad, p.existencia, p.precio FROM pedidos e, productos p WHERE e.codigo_producto = p.codigo_producto AND codigo_pedidos = ?";
	$params = array("%$search%");
}

try
{
	$data = Database::getRows($sql, $params);
    if($data != null)
    {
        print("
        <div class= 'container'>
        <table class='striped centered'>
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio Unitario</th>
                        <th>Total</th>
                        <th>Modificar</th>
                        <th>Quitar</th>
                    </tr>
                </thead>
		    <tbody>
        ");
        
        foreach($data as $row)
        {
			$price = ["precio"];
            print
            
            ("
                <tr>
                    <td>".$row['nombre_producto']."</td>
                    <td>".$row['cantidad']."</td>
                    <td>".$row['precio']."</td>
					<td>".$row["cantidad"]*$price."</td>
                    <td>
                        <a href='pedidos.php?id=".$row['codigo_pedido']."' class='blue-text'><span class='icon-spinner3'></span>Editar</a>
                    </td>
                    <td>
                        <a href='nocarrito.php?id=".$row['codigo_pedido']."' class='blue-text'><span class='icon-bin'></span>Eliminar</a>
                    </td>
            ");
            
        }
        print("
                </tr>
		    </tbody>
	    </table>
          <a href='detalletotal.php' class='btn waves-effec white-text'><span class='icon-coin-dollar'></span>Pagar</a>  
        </div>
	    ");     
    }
    else
    {
        Page::showMessage(4, "No has agregado productos al carrito", "index.php");
    }
}
catch(Exception $error)
{
	Page::showMessage(2, $error->getMessage(), "index.php");
}

?>
<?php
Page::footer();
?>
