<?php
require("page.php");
Page::header("Restablecer Contraseña");

if(empty($_GET['id'])) 
{
    $correo = null;
    $alias = null;
}

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
    $alias = $_POST['alias'];
    $correo = $_POST['correo'];
    $clave1 = $_POST['clave1'];
    $clave2 = $_POST['clave2'];
$sql2 = "SELECT email_cliente FROM usuarios_clientes WHERE nombre_usuariocliente = '$alias'";
$data = Database::getRow($sql2, null);
$compro = $data['email_cliente'];
try 
{
    if($correo != "")
    {
            if($alias != "")
            {
                $clave1 = $_POST['clave1'];
                $clave2 = $_POST['clave2'];
                if($clave1 != "" && $clave2 != "")
                {
                    if($clave1 == $clave2)
                    {
                        if($correo == $compro)
                        {
                            $clave = password_hash($clave1, PASSWORD_DEFAULT);
                            $sql = "UPDATE usuarios_clientes SET clave = ? WHERE nombre_usuariocliente = ?";
                            $params = array($clave1, $alias);
                            header("location: index.php");
                        }
                    }
                    else
                    {
                        throw new Exception("Las contraseñas no coinciden");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar dos veces la contraseña");
                }
            }
            else
            {
                throw new Exception("Debe ingresar el alias");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el correo electrónico");
        }
}        
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<h2 class='center'>Restablecer Contraseña</h2>
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6 offset-m3'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='alias' type='text' name='alias' class='validate' value='<?php print($alias);?>' required/>
            <label for='alias'>Alias</label>
        </div>
        <div class='input-field col s12 m6 offset-m3'>
            <i class='material-icons prefix'>email</i>
            <input id='correo' type='email' name='correo' class='validate' value='<?php print($correo);?>' required/>
            <label for='correo'>Correo</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6 offset-m3'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6 offset-m3'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='login.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
 	    <button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
    </div>
</form>

<?php
Page::footer();
?>