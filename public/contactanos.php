<?php
require("page.php");
if(empty($_GET['id'])) 
{
    Page::header("");
    $id = null;
    $nombre = null;
    $descripcion = null;
    $cliente = $_SESSION['codigo_usuariocliente'];
    $fecha = date('Y/m/d');
    $estado = 2;
    $producto = null;
    $valoracion = 5;
}
else
{
    Page::header("");
    $id = $_GET['id'];
    $sql = "SELECT * FROM comentarios WHERE codigo_comentario = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $cliente = $data['codigo_usuariocliente'];
    $nombre = $data['nombre_comentario'];
    $descripcion = $data['descripcion_comentario'];
    $fecha = $data['fecha'];
    $estado = $data['estado'];
    $producto = $data['codigo_producto'];
    $valoracion = $data['valoracion'];
}

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$cliente = $_POST['cliente'];
    $nombre = $_POST['nombre'];
  	$descripcion = $_POST['descripcion'];
    $producto = $_POST['producto'];

    try 
    {
      	if($nombres != "")
        {
            if($descripcion != "")
            {
                if($producto != "")
                {
                if($id == null)
                {
                   $sql = "INSERT INTO comentarios(codigo_usuariocliente, nombre_comentario, descripcion_comentario, fecha, estado, codigo_producto, valoracion) VALUES(?, ?, ?, ?, ?, ?, ?)";
                   $params = array($cliente, $nombre, $descripcion, $fecha, $estado, $producto, $valoracion);
                            }
                else
                {
                    $sql = "UPDATE comentarios SET codigo_usuariocliente = ?, nombre_comentario = ?, descripcion_comentario = ?, estado = ?, codigo_producto = ?, valoracion = ? WHERE codigo_comentario = ?";
                    $params = array($cliente, $nombre, $descripcion, $estado, $producto, $valoracion, $id);
                }
                Database::executeRow($sql, $params);
                header("location: index.php");
            }
            else
            {
                throw new Exception("Debe seleccionar un producto");
            }
        }
        else
            {
                throw new Exception("Debe ingresar una descripcion");
            }
        }
        else
        {
            throw new Exception("Debe ingresar un asunto");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container ">
        <br><br>
        <h1 class="header center"><img src="../img/logo1.png" width="300" height="250"></h1>
        <div class="row center">
        </div>
      </div>
    </div>
    <div class="parallax"><img src="../img/wallpaper2.jpg" alt="Unsplashed background img 1"></div>
  </div>
  <h1 class='center'>Dejanos tu Comentario</h1>
<form method='post'>
	<div class='row'>
        <div class='input-field col s12 m6 offset-m3'>
          	<i class='material-icons prefix'>person</i>
          	<input id='cliente' type='text' name='cliente' class='validate' value='<?php print($_SESSION['nombre_usuariocliente']); ?>' required/>
          	<label>Nombre</label>
        </div>
		<div class='input-field col s12 m6 offset-m3'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
          	<label for='nombre'>Asunto</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6 offset-m3'>
          	<i class='material-icons prefix'>person</i>
            <textarea id='descripcion' name='descripcion' class='materialize-textarea' data-length='100' value='<?php print($descripcion); ?>' required/></textarea>
          	<label for='descripcion'>Descripcion</label>
        </div>
    </div>
    <div class='row center-align'>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<div class="fb-comments" data-href="https://www.facebook.com/Cute-Store-1362167270520408/" data-numposts="7"></div>

<?php
Page::footer();
?>