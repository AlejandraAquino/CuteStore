<?php
require("../inc/database.php");
require("../inc/validator.php");
class Page
{
	public static function header($title)
	{
		session_start();
		ini_set("date.timezone","America/El_Salvador");
		print("
			<!DOCTYPE html>
			<html lang='es'>
			<head>
				<meta charset='utf-8'>
				<title>Cliente - $title</title>
				<link type='text/css' rel='stylesheet' href='../css/materialize.min.css'/>
				<link type='text/css' rel='stylesheet' href='../css/sweetalert2.min.css'/>
				<link type='text/css' rel='stylesheet' href='../css/icons.css'/>
				<script type='text/javascript' src='../js/sweetalert2.min.js'></script>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
			</head>
			<body>
		");

	if(isset($_SESSION['nombre_usuariocliente']))
{
	print("
	<header class='navbar-fixed'>
		<nav class='teal lighten-2'>
			<div class='nav-wrapper teal lighten-2'>
				<a href='index.php' class='brand-logo center'>CuteStore</a>
				<a href='#' data-activates='mobile' class='button-collapse'><i class='material-icons'>menu</i></a>
				<ul class='hide-on-med-and-down'>
					<li class='left'><a href='index.php'> Home <i class='material-icons right'>store</i></a></li>
					<li><a href='productos.php' >Productos<i class='material-icons right'>grade</i></a></li>
					<li><a href='contactanos.php' >contactanos<i class='material-icons right'>grade</i></a></li>
					<li><a href='proceso.php'><i class='material-icons right'>library_books</i>Carrito<li>
					<li class='right'><a class='dropdown-button' href='#' data-activates='dropdown1'><i class='material-icons left'>verified_user</i>".$_SESSION['nombre_usuariocliente']."</a></li>
				</ul>
				<ul id='dropdown1' class='dropdown-content'>
					<li><a href='profile.php'><i class='material-icons left'>edit</i>Editar perfil</a></li>
					<li><a href='logout.php'><i class='material-icons left'>clear</i>Salir</a></li>
				</ul>
			</div>
		</nav>
	</header>
		<ul class='side-nav' id='mobile'>
			<li><a href='index.php'> Home <i class='material-icons right'>store</i></a></li>
			<li><a href='productos.php' >Productos<i class='material-icons right'>grade</i></a></li>
			<li><a href='contactanos.php' >contactanos<i class='material-icons right'>grade</i></a></li>
			<li><a href='proceso.php'><i class='material-icons right'>library_books</i>Carrito<li>
			<li><a class='dropdown-button' href='#' data-activates='dropdown'><i class='material-icons left'>verified_user</i>".$_SESSION['nombre_usuariocliente']."</a></li>
		</ul>
		<ul id='dropdown' class='dropdown-content'>
			<li><a href='profile.php'><i class='material-icons left'>edit</i>Editar perfil</a></li>
			<li><a href='logout.php'><i class='material-icons left'>clear</i>Salir</a></li>
		</ul>
		<main class=''>
			<h3 class='center-align'>".$title."</h3>
	");
		}
		else
		{
			print("
			<header class='navbar-fixed'>
			<nav>
				<div class='nav-wrapper teal lighten-2'>
					<a href='#' data-activates='mobile' class='button-collapse'><i class='material-icons'>menu</i></a>
					<a href='#' class='brand-logo center'>CuteStore</a>					
					<ul class='hide-on-med-and-down'>
            			<li class='left'><a href='index.php'> Home <i class='material-icons right'>store</i></a></li>
						<li><a href='productos.php' >Productos<i class='material-icons right'>grade</i></a></li>
						<li class='right'><a href='login.php'>Iniciar Sesion  <i class='material-icons right'>group_work</i></a></li>
					</ul>
				</div>
			</nav>
		</header>

	<ul class='side-nav left' id='mobile'>
		<li class='left'><a href='index.php'> Home <i class='material-icons right'>store</i></a></li>
		<li><a href='productos.php' >Productos<i class='material-icons right'>grade</i></a></li>
		<li class='right'><a href='login.php'>Iniciar Sesion  <i class='material-icons right'>group_work</i></a></li>
	</ul>
		<main class=''>
			");
		}
	}

	public static function footer()
	{
		print("
			</main>
			 <footer class='page-footer teal lighten-2'>
				<div class='container'>
					<div class='row'>
					<div class='col l6 s12'>
							<div class='fb-like' data-href='https://www.facebook.com/Cute-Store-1362167270520408/' 
							data-layout='standard' data-action='like' data-size='small' data-show-faces='true' data-share='true'></div>
							
					</div>
					<div class='col l4 offset-l2 s12'>
				<div class='fb-send' data-href='https://www.facebook.com/Cute-Store-1362167270520408/notifications/'></div>

					</div>
					</div>
				</div>
				<div class='footer-copyright  blue accent-1'>
					<div class='container black-text'>
					© 2017 Copyright
					</div>
				</div>
			</footer>
			<script type='text/javascript' src='../js/jquery-2.1.1.min.js'></script>
			<script type='text/javascript' src='../js/materialize.min.js'></script>
			<script type='text/javascript' src='../js/valiFra.js'></script>
			<script type='text/javascript' src='js/inicializacion.js'></script>
			</body>
			</html>
		");
	}

	public static function setCombo($label, $name, $value, $query)
	{
		$data = Database::getRows($query, null);
		print("<select name='$name' required>");
		if($data != null)
		{
			if($value == null)
			{
				print("<option value='' disabled selected>Seleccione una opción</option>");
			}
			foreach($data as $row)
			{
				if(isset($_POST[$name]) == $row[0] || $value == $row[0])
				{
					print("<option value='$row[0]' selected>$row[1]</option>");
				}
				else
				{
					print("<option value='$row[0]'>$row[1]</option>");
				}
			}
		}
		else
		{
			print("<option value='' disabled selected>No hay registros</option>");
		}
		print("
			</select>
			<label>$label</label>
		");
	}

	public static function showMessage($type, $message, $url)
	{
		$text = addslashes($message);
		switch($type)
		{
			case 1:
				$title = "Éxito";
				$icon = "success";
				break;
			case 2:
				$title = "Error";
				$icon = "error";
				break;
			case 3:
				$title = "Advertencia";
				$icon = "warning";
				break;
			case 4:
				$title = "Aviso";
				$icon = "info";
		}
		if($url != null)
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false}).then(function(){location.href = '$url'})</script>");
		}
		else
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false})</script>");
		}
	}
}
?>