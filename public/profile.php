<?php
require("page.php");
Page::header("Editar perfil");

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres_clientes'];
  	$apellidos = $_POST['apellidos_clientes'];
    $alias = $_POST['nombre_usuariocliente'];
    $correo = $_POST['email_cliente'];
  	$direccion = $_POST['direccion'];
  	$telefono = $_POST['numero_telefonico'];
    $clave1 = $_POST['clave1'];
    $clave2 = $_POST['clave2'];

try 
{
    if($nombres != "" && $apellidos != "")
    {
        if($alias != "")
        {
            if($correo != "")
            {
                if($direccion != "")
                {
                    if($telefono != "")
                    {
                    if($clave1 != "" || $clave2 != "")
                    {
                        if($clave1 == $clave2)
                        {
                            $clave = password_hash($clave1, PASSWORD_DEFAULT);
                            $sql = "UPDATE usuarios_clientes SET nombres_clientes = ?, apellidos_clientes = ?, nombre_usuariocliente = ?, email_cliente = ?, direccion = ?, numero_telefonico = ?, clave = ? WHERE codigo_usuariocliente = ?";
                            $params = array($nombres, $apellidos, $alias, $correo, $direccion, $telefono, $clave, $_SESSION['codigo_usuariocliente']);
                        }
                        else
                        {
                            throw new Exception("Las contraseñas no coinciden");
                        }
                    }
                    else
                    {
                        $sql = "UPDATE usuarios_clientes SET nombres_clientes = ?, apellidos_clientes = ?, nombre_usuariocliente = ?, email_cliente = ?, direccion = ?, numero_telefonico = ? WHERE codigo_usuariocliente = ?";
                        $params = array($nombres, $apellidos, $alias, $correo, $direccion, $telefono, $_SESSION['codigo_usuariocliente']);
                    }
                    Database::executeRow($sql, $params);
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                        else
                        {
                            throw new Exception("Debe ingresar un numero telefonico");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar una direccion");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar un correo electrónico");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un usuario");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $sql = "SELECT nombres_clientes, apellidos_clientes, nombre_usuariocliente, email_cliente, direccion, numero_telefonico FROM usuarios_clientes WHERE codigo_usuariocliente = ?";
    $params = array($_SESSION['codigo_usuariocliente']);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_clientes'];
    $apellidos = $data['apellidos_clientes'];
    $alias = $data['nombre_usuariocliente'];
    $correo = $data['email_cliente'];
    $direccion = $data['direccion'];
    $telefono = $data['numero_telefonico'];
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres_clientes' type='text' name='nombres_clientes' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres_clientes'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos_clientes' type='text' name='apellidos_clientes' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos_clientes'>Apellidos</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='nombre_usuariocliente' type='text' name='nombre_usuariocliente' class='validate' value='<?php print($alias); ?>' required/>
            <label for='nombre_usuariocliente'>Alias</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='email_cliente' type='email' name='email_cliente' class='validate' value='<?php print($correo); ?>' required/>
            <label for='email_cliente'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>location_on</i>
            <input id='direccion' type='text' name='direccion' class='validate' value='<?php print($direccion); ?>' required/>
            <label for='direccion'>Direccion</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>call</i>
            <input id='numero_telefonico' type='text' name='numero_telefonico' class='validate' value='<?php print($telefono); ?>' required/>
            <label for='numero_telefonico'>Telefono</label>
        </div>
    </div>
    <div class='row center-align'>
        <label>CAMBIAR CLAVE</label>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate'/>
            <label for='clave1'>Contraseña nueva</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate'/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect blue'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>