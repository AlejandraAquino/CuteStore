<?php
require("../inc/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar Proveedor");
    $id = null;
    $nombres = null;
    $telefono = null;
    $correo = null;
}
else
{
    Page::header("Modificar Proveedor");
    $id = $_GET['id'];
    $sql = "SELECT * FROM proveedor WHERE codigo_proveedor = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombre_proveedor'];
    $telefono = $data['telefono_proveedor'];
    $correo = $data['email_proveedor'];
}

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres'];
  	$telefono = $_POST['telefono'];
    $correo = $_POST['correo'];

    try 
    {
      	if($nombres != "")
        {
            if($telefono != "")
            {
                if($correo != "")
                {
                if($id == null)
                {
                   $sql = "INSERT INTO proveedor(nombre_proveedor, telefono_proveedor, email_proveedor) VALUES(?, ?, ?)";
                   $params = array($nombres, $telefono, $correo);
                            }
                else
                {
                    $sql = "UPDATE proveedor SET nombre_proveedor = ?, telefono_proveedor = ?, email_proveedor = ? WHERE codigo_proveedor = ?";
                    $params = array($nombres, $telefono, $correo, $id);
                }
                Database::executeRow($sql, $params);
                header("location: index.php");
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico");
            }
        }
        else
            {
                throw new Exception("Debe ingresar el telefono");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres' type='text' name='nombres' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>phone</i>
            <input id='telefono' type="text" maxlength="8" size="8" name='telefono' class='validate'  value='<?php print($telefono); ?>' required/>
            <label for='telefono'>Telefono</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='correo' type='email' name='correo' class='validate' value='<?php print($correo); ?>' required/>
            <label for='correo'>Correo</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>