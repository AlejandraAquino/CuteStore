<?php
require("../inc/page.php");
Page::header("Productos");?>
<div class='input-field col s6 m4 center'>
			 <i class="large material-icons">shop</i>
		</div>

<form method='post'>
		<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<?php

if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM productos p, tipo_productos t, proveedor e WHERE p.codigo_tipoproducto = t.codigo_tipoproducto AND p.codigo_proveedor = e.codigo_proveedor LIKE ? ORDER BY nombre_producto";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM productos p, tipo_productos t, proveedor e WHERE p.codigo_tipoproducto = t.codigo_tipoproducto AND p.codigo_proveedor = e.codigo_proveedor ORDER BY nombre_producto";
	$params = null;
}
try
{
	
	$data = Database::getRows($sql, $params);
	if($data != null)
	{
		print("
			<table class='striped'>
				<thead>
					<tr>
						<th>IMAGEN</th>
						<th>NOMBRE</th>
						<th>PRECIO ($)</th>
						<th>TIPO</th>
						<th>PROVEEDOR</th>
						<th>ESTADO</th>
						<th>ACCIÓN</th>
					</tr>
				</thead>
				<tbody>
		");
	foreach($data as $row)
	{
		print("
			<tr>
				<td><img src='../../img/productos/".$row['imagen']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['nombre_producto']."</td>
				<td>".$row['precio']."</td>
				<td>".$row['tipo_producto']."</td>
				<td>".$row['nombre_proveedor']."</td>
				<td>
		");
		if($row['codigo_estadoproducto'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['codigo_producto']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_producto']."&image=".$row['imagen']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
	else
	{
		Page::showMessage(4, "No hay registros disponibles", "save.php");
	}
}

	catch(Exception $error)
	{
		Page::showMessage(2, $error->getMessage(), "../main/");
	}

Page::footer();
?>