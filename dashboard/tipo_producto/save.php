<?php
require("../inc/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar Tipo producto");
    $id = null;
    $nombre = null;
    $descripcion = null;
}
else
{
    Page::header("Modificar Tipo producto");
    $id = $_GET['id'];
    $sql = "SELECT * FROM tipo_productos WHERE codigo_tipoproducto = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombre = $data['tipo_producto'];
    $descripcion = $data['description'];
}

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre'];
  	$descripcion = $_POST['descripcion'];
    if($descripcion == "")
    {
        $descripcion = null;
    }

    try 
    {
      	if($nombre != "")
        {
            if($id == null)
            {
                $sql = "INSERT INTO tipo_productos (tipo_producto, description) VALUES(?, ?)";
                $params = array($nombre, $descripcion);
            }
            else
            {
                $sql = "UPDATE tipo_productos SET tipo_producto = ?, description = ? WHERE codigo_tipoproducto = ?";
                $params = array($nombre, $descripcion, $id);
            }
            Database::executeRow($sql, $params);
            header("location: index.php");
        }
        else
        {
            throw new Exception("Debe digitar nombre del tipo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>note_add</i>
            <input id='tipo_usuario' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
            <label for='nombre'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>description</i>
            <input id='description' type='text' name='descripcion' class='validate' value='<?php print($descripcion); ?>'/>
            <label for='descripcion'>Descripción</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>