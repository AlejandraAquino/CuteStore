<?php
require("../inc/page.php");
Page::header("Editar perfil");

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres_usuario'];
  	$apellidos = $_POST['apellidos_usuario'];
    $alias = $_POST['nombre_usuario'];
    $correo = $_POST['email_usuario'];
    $clave1 = $_POST['clave1'];
    $clave2 = $_POST['clave2'];

    try
    {
      	if($nombres != "" && $apellidos != "")
        {
            if($alias != "")
            {
                if($correo != "")
                {
                    if($clave1 != "" || $clave2 != "")
                    {
                        if($clave1 == $clave2)
                        {
                            $clave = password_hash($clave1, PASSWORD_DEFAULT);
                            $sql = "UPDATE usuarios SET nombres_usuario = ?, apellidos_usuario = ?, nombre_usuario = ?, email_usuario = ?, clave = ? WHERE codigo_usuario = ?";
                            $params = array($nombres, $apellidos, $alias, $correo, $clave, $_SESSION['codigo_usuario']);
                        }
                        else
                        {
                            throw new Exception("Las contraseñas no coinciden");
                        }
                    }
                    else
                    {
                        $sql = "UPDATE usuarios SET nombres_usuario = ?, apellidos_usuario = ?, nombre_usuario = ?, email_usuario = ? WHERE codigo_usuario = ?";
                        $params = array($nombres, $apellidos, $alias, $correo, $_SESSION['codigo_usuario']);
                    }
                    Database::executeRow($sql, $params);
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                else
                {
                    throw new Exception("Debe ingresar un correo electrónico");
                }
            }
            else
            {
                throw new Exception("Debe ingresar un usuario");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
else
{
    $sql = "SELECT nombres_usuario, apellidos_usuario, nombre_usuario, email_usuario FROM usuarios WHERE codigo_usuario = ?";
    $params = array($_SESSION['codigo_usuario']);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_usuario'];
    $apellidos = $data['apellidos_usuario'];
    $alias = $data['nombre_usuario'];
    $correo = $data['email_usuario'];
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres_usuario' type='text' name='nombres_usuario' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres_usuario'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos_usuario' type='text' name='apellidos_usuario' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos_usuario'>Apellidos</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='nombre_usuario' type='text' name='nombre_usuario' class='validate' value='<?php print($alias); ?>' required/>
            <label for='nombre_usuario'>Alias</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='email_usuario' type='email' name='email_usuario' class='validate' value='<?php print($correo); ?>' required/>
            <label for='email_usuario'>Correo</label>
        </div>
    </div>
    <div class='row center-align'>
        <label>CAMBIAR CLAVE</label>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate'/>
            <label for='clave1'>Contraseña nueva</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate'/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='../main/index.php' class='btn waves-effect blue'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>