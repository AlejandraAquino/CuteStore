<?php
require("../inc/page.php");
Page::header("Facturas");?>
<div class='input-field col s6 m4 center'>
			 <i class="large material-icons">shop</i>
		</div>
<?php

if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM factura f, usuarios_clientes c WHERE  f.codigo_usuariocliente = c.codigo_usuariocliente LIKE ? ORDER BY codigo_factura";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM factura f, usuarios_clientes c WHERE  f.codigo_usuariocliente = c.codigo_usuariocliente ORDER BY codigo_factura";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>

<table class='striped'>
	<thead>
		<tr>
			<th>TOTAL</th>
			<th>CLIENTE</th>
			<th>FECHA</th>
			<th>ESTADO</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['total_factura']."</td>
				<td>".$row['nombres_clientes']."</td>
				<td>".$row['fecha_factura']."</td>
				<td>".$row['estado_factura']."</td>
				<td>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "index.php");
}
Page::footer();
?>