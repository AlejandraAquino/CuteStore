<?php
require("../inc/page.php");
Page::header("Comentarios");?>

<div class='input-field col s8 m6 center'>
			 <i class="large material-icons">shop</i>
		</div>
<?php

if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM comentarios c, usuarios_clientes u, productos p WHERE  c.codigo_usuariocliente = u.codigo_usuariocliente AND c.codigo_producto = p.codigo_producto LIKE ? ORDER BY codigo_comentario";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM comentarios c, usuarios_clientes u, productos p WHERE c.codigo_usuariocliente = u.codigo_usuariocliente AND c.codigo_producto = p.codigo_producto ORDER BY codigo_comentario";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>

<table class='striped'>
	<thead>
		<tr>
			<th>CLIENTE</th>
			<th>NOMBRE</th>
			<th>DESCRIPCIÓN</th>
			<th>FECHA</th>
            <th>ESTADO</th>
            <th>PRODUCTO</th>
            <th>VALORACIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['nombres_clientes']."</td>
				<td>".$row['nombre_comentario']."</td>
				<td>".$row['descripcion_comentario']."</td>
				<td>".$row['fecha']."</td>
				<td>
		");
		if($row['estado'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}

		print("<td>".$row['nombre_producto']."</td>
			   <td>
			");

		if($row['valoracion'] == 1)
		{
			print("<i class='material-icons left'>star</i>");
		}
		else
		{
			print("<i class='material-icons left'>star</i><i class='material-icons left'>star</i><i class='material-icons left'>star</i><i class='material-icons left'>star</i><i class='material-icons left'>star</i>");
		}
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "index.php");
}
Page::footer();
?>