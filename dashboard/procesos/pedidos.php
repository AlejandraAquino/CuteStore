<?php
require("../inc/page.php");
Page::header("Pedidos");?>
<div class='input-field col s6 m4 center'>
			 <i class="large material-icons">shop</i>
		</div>
<?php

if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM pedidos p, productos o WHERE  p.codigo_producto = o.codigo_producto LIKE ? ORDER BY codigo_pedidos";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM pedidos p, productos o WHERE p.codigo_producto = o.codigo_producto ORDER BY codigo_pedidos";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>

<table class='striped'>
	<thead>
		<tr>
			<th>N* FACTURA</th>
			<th>CANTIDAD</th>
			<th>FECHA</th>
			<th>PRODUCTO</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['codigo_factura']."</td>
				<td>".$row['cantidad']."</td>
				<td>".$row['fecha_pedido']."</td>
				<td>".$row['nombre_producto']."</td>
				<td>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "index.php");
}
Page::footer();
?>