<?php
require("../inc/page.php");
Page::header("Usuarios");?>
<div class='input-field col s6 m4 center'>
			 <i class="large material-icons">group</i>
		</div>
<?php
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM usuarios u, tipo_usuarios t WHERE u.codigo_tipousuario = t.codigo_tipousuario LIKE ? OR nombres_usuario LIKE ? ORDER BY apellidos_usuario";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM usuarios u, tipo_usuarios t WHERE u.codigo_tipousuario = t.codigo_tipousuario ORDER BY apellidos_usuario";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>

<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>APELLIDOS</th>
			<th>NOMBRES</th>
			<th>CORREO</th>
			<th>USUARIO</th>
			<th>TIPO USUARIO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['apellidos_usuario']."</td>
				<td>".$row['nombres_usuario']."</td>
				<td>".$row['email_usuario']."</td>
				<td>".$row['nombre_usuario']."</td>
				<td>".$row['tipo_usuario']."</td>
				<td>
					<a href='save.php?id=".$row['codigo_usuario']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['codigo_usuario']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} 
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>