<?php
require("../inc/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar usuario");
    $id = null;
    $nombres = null;
    $apellidos = null;
    $correo = null;
    $alias = null;
    $tipo = null;
}
else
{
    Page::header("Modificar usuario");
    $id = $_GET['id'];
    $sql = "SELECT * FROM usuarios WHERE codigo_usuario = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombres_usuario'];
    $apellidos = $data['apellidos_usuario'];
    $correo = $data['email_usuario'];
    $alias = $data['nombre_usuario'];
    $tipo = $data['codigo_tipousuario'];
}

if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres'];
  	$apellidos = $_POST['apellidos'];
    $correo = $_POST['correo'];
    $tipo = $_POST['tipo_usuario'];

    try 
    {
        if($nombres != "" && $apellidos != "")
        {
            if($correo != "")
            {
                if($tipo != "")
                {
                    if($id == null)
                    {
                        $alias = $_POST['alias'];
                    if($alias != "")
                    {
                        $clave1 = $_POST['clave1'];
                        $clave2 = $_POST['clave2'];
                        if($clave1 != "" && $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO usuarios (nombres_usuario, apellidos_usuario, nombre_usuario, email_usuario, clave, codigo_tipousuario) VALUES(?, ?, ?, ?, ?, ?)";
                                $params = array($nombres, $apellidos, $alias, $correo, $clave, $tipo);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un nombre de usuario");
                    }
                }
                else
                {
                    $sql = "UPDATE usuarios SET nombres_usuario = ?, apellidos_usuario = ?, email_usuario = ?, codigo_tipousuario = ? WHERE codigo_usuario = ?";
                    $params = array($nombres, $apellidos, $correo, $tipo, $id);
                }
                Database::executeRow($sql, $params);
                header("location: index.php");
                }
            else
            {
                throw new Exception("Debe seleccionar un tipo de usuario"); 
            }
        }
        else
            {
                throw new Exception("Debe ingresar un correo electrónico");
            }
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres_usuario' type='text' name='nombres' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos_usuario' type='text' name='apellidos' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos'>Apellidos</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='email_usuario' type='email' name='correo' class='validate' value='<?php print($correo); ?>' required/>
            <label for='correo'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='nombre_usuario' type='text' name='alias' class='validate' <?php print("value='$alias' "); print(($id == null)?"required":"disabled"); ?>/>
            <label for='alias'>Alias</label>
        </div>

        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>perm_identity</i>
            <?php
            $sql = "SELECT codigo_tipousuario,tipo_usuario FROM tipo_usuarios";
            Page::setCombo("Tipo usuario", "tipo_usuario", $tipo, $sql);
            ?>
        </div>
    </div>
    <?php
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>