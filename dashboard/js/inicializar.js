$(document).ready(function(){
    $('.button-collapse').sideNav();
    $('.materialboxed').materialbox();
    $('select').material_select();
    $(".dropdown-button").dropdown({hover: true, belowOrigin: true});
    $('select').material_select();
    $('.parallax').parallax();
});

$(function(){
    //Para escribir solo letras
    $('#Cam1').validCampoFranz(' abcdefghijklmnñopqrstuvwxyz');

    //Para escribir solo numeros    
    $('#Cam2').validCampoFranz('0123456789');    
});