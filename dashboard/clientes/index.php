<?php
require("../inc/page.php");
Page::header("Clientes");?>
<div class='input-field col s6 m4 center'>
			 <i class="large material-icons">contact_phone</i>
		</div>
<?php
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM usuarios_clientes LIKE ? OR nombres_clientes LIKE ? ORDER BY codigo_usuariocliente";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM usuarios_clientes ORDER BY codigo_usuariocliente";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>

<table class='striped'>
	<thead>
		<tr>
			<th>APELLIDOS</th>
			<th>NOMBRES</th>
			<th>USUARIO</th>
			<th>CORREO</th>
			<th>DIRECCIÓN</th>
			<th>TELEFONO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['apellidos_clientes']."</td>
				<td>".$row['nombres_clientes']."</td>
				<td>".$row['nombre_usuariocliente']."</td>
				<td>".$row['email_cliente']."</td>
				<td>".$row['direccion']."</td>
				<td>".$row['numero_telefonico']."</td>
				<td>
					<a href='save.php?id=".$row['codigo_usuariocliente']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['codigo_usuariocliente']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} 
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>